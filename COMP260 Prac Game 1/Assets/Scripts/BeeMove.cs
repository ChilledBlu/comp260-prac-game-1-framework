﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	private float speed;
	private float turnSpeed;

	private Transform target;
	private Vector2 heading;


	// Use this for initialization
	void Start () {

		PlayerMove p = FindObjectOfType<PlayerMove> ();
		target = p.transform;
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		speed = Mathf.Lerp (minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);
		
	}

	void OnDrawGizmos(){
		
	}


	public ParticleSystem explosionPrefab;

	void OnDestroy() {
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		Destroy (explosion.gameObject, explosion.main.duration);
	}

	// Update is called once per frame
	void Update () {

		Vector2 direction = target.position - transform.position;

		float angle = turnSpeed * Time.deltaTime;

		if (direction.IsOnLeft (heading)) {
			heading = heading.Rotate (angle);
		} else {
			heading = heading.Rotate (-angle);
		}

		transform.Translate (heading * speed * Time.deltaTime);
	}
}
