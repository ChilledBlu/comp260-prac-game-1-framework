﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

	public BeeMove beePrefab;
	public PlayerMove player;
	public int nBees = 50;
	public float xMin, yMin;
	public float width, height;

	public float minBeePeriod = 3.0f; 
	public float maxBeePeriod = 5.0f;
	private float countdown = 0.0f;

	float beePeriod;

	// Use this for initialization
	void Start () {
		
		for (int i = 0; i < nBees; i++) {
			
			BeeMove bee = Instantiate (beePrefab);
			bee.transform.parent = transform;
			bee.gameObject.name = "bee " + i;

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);

		}



	}

	public void DestroyBees (Vector2 centre, float radius) {
		for (int i = 0; i < transform.childCount; i++){
			Transform child = transform.GetChild(i);
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius){
				Destroy(child.gameObject);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		countdown -= Time.deltaTime;
		if(countdown <= 0.0f){
			BeeMove bee = Instantiate (beePrefab);
			bee.transform.parent = transform;
			bee.gameObject.name = "countdown bee";

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
			countdown = Random.Range (minBeePeriod, maxBeePeriod);
	}
}
}