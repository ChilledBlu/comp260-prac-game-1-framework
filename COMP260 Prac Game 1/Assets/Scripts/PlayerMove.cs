﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public string horizontalAxis;
	public string verticalAxis;
	// Use this for initialization

	private BeeSpawner beeSpawner;
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}

	public float maxSpeed = 5.0f;
	public float acceleration = 10.0f;
	public float brake = 1000.0f;

	public float turnSpeed = 30.0f;
	private float speed = 0.0f;
	// Update is called once per frame

	public float destroyRadius = 1.0f;

	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			beeSpawner.DestroyBees (
				transform.position, destroyRadius);
		}

		float turn = Input.GetAxis ("Horizontal");
		turnSpeed = speed * 10;
		transform.Rotate (0, 0, -turn * turnSpeed * Time.deltaTime);
		brake = Mathf.Clamp (brake, 0.0f, maxSpeed);
		float forwards = Input.GetAxis ("Vertical");
		if (forwards > 0){
			speed = speed + acceleration * Time.deltaTime;
		}else {
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else {
				speed = speed + brake * Time.deltaTime;
			}
	}

		speed = Mathf.Clamp (speed, -maxSpeed, maxSpeed);

		Vector2 direction;
		direction.x = Input.GetAxis(horizontalAxis);
		direction.y = Input.GetAxis (verticalAxis);

		Vector2 velocity = Vector2.up * speed;
		transform.Translate(velocity *  Time.deltaTime, Space.Self);
		
	}
}
